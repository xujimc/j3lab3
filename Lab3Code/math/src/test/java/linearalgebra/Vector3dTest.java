//Jimmy Xu 2138599
package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class Vector3dTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void dotProductTest()
    {
        Vector3d test1 = new Vector3d(2,3,4);
        Vector3d test2 = new Vector3d(5,6,7);
        assertEquals(56,test1.dotProduct(test2),0);
    }

    @Test
    public void testGetters(){
        Vector3d test = new Vector3d(2,3,4);
        assertEquals(2,test.getX(),0);
        assertEquals(3,test.getY(),0);
        assertEquals(4,test.getZ(),0);
    }

    @Test
    public void getMagnitudeTest(){
        Vector3d test = new Vector3d(2,3,4);
        assertEquals(Math.sqrt(29),test.getMagnitude(),0);
        assertEquals(5.38516480713,test.getMagnitude(),0.00000000001);
    }

    @Test
    public void addTest(){
        Vector3d test1 = new Vector3d(2,3,4);
        Vector3d test2 = new Vector3d(5,6,7);
        Vector3d output = test1.add(test2);
        assertEquals(7,output.getX(),0);
        assertEquals(9,output.getY(),0);
        assertEquals(11,output.getZ(),0);

    }
}
