//Jimmy Xu 2138599
package linearalgebra;

/**
 * Hello world!
 *
 */
public class Vector3d 
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double getMagnitude(){
        return Math.sqrt(x*x+y*y+z*z);//Math.pow would make this unreadable
    }

    public double dotProduct(Vector3d input){
        return (x*input.x+y*input.y+z*input.z);
    }

    public Vector3d add(Vector3d input){
        double new_x=this.x+input.x;
        double new_y=this.y+input.y;
        double new_z=this.z+input.z;
        return (new Vector3d(new_x,new_y,new_z));
    }
}
